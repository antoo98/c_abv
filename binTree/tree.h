#ifndef TREE_H
#define TREE_H

struct t_node {
    int* elem;
	struct t_node* l;
    struct t_node* r;
};

void log_ (char*);
void log_i(int);

void t_insert(struct t_node*, int*);
int t_contains(struct t_node*, int*);
void t_print(struct t_node*);

#endif
