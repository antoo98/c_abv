#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

void log_ (char* text) {printf("\033[32m%s\033[0m\n", text );}
void log_i(int value ) {printf("\033[33m %d \033[0m\n", value);}


void t_insert(struct t_node* root, int* elem) {
	if (root == NULL) return;

	struct t_node** next = NULL;
	if (root->elem == NULL) {
		root->elem = elem;
		return;
	}
	if (*elem < *(root->elem)) {
		next = &(root->l);
	} else if (*elem > *(root->elem)) {
		next = &(root->r);
	} else {
		return; //no duplicates
	}
	if (*next == NULL) {
		*next = calloc(1, sizeof(struct t_node));
		(*next)->elem = elem;
	} else {
		t_insert(*next, elem);
	}
}

int t_contains(struct t_node* root, int* elem) {
	if (root == NULL || root->elem == NULL) return 0;
	if (*root->elem == *elem) return 1;
	if (*root->elem > *elem) return t_contains(root->l, elem);
	return t_contains(root->r, elem);
}

void t_print(struct t_node* root) {
	if (root == NULL) return;
	t_print(root->l);
	printf("%d ", *(root->elem));
	t_print(root->r);
}
