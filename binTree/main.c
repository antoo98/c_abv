#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int main(int argc, char* argv[]) {
	struct t_node root = {.elem = NULL, .l = NULL, .r = NULL};
	int val[] = {5, 6, 4, 7, 3, 9, 15};

	for (size_t i = 0; i < (sizeof(val)/sizeof(int)); i++) {
		t_insert(&root, &val[i]);
	}

	log_("initial tree:");
	t_print(&root);
	printf("\n");

	log_("tree contains 5?");
	int val5 = 5;
	log_i(t_contains(&root, &val5));
	printf("\n");

	log_("tree contains 10?");
	int val10 = 10;
	log_i(t_contains(&root, &val10));
	printf("\n");

}
