#include <stdio.h>
#include <stdlib.h>
#include "list.h"

void log_ (char* text) {printf("\033[32m%s\033[0m\n", text );}
void log_i(int value ) {printf("\033[33m %d \033[0m\n", value);}


void add(struct list_elem* head, void* elem) {
	if (head == NULL) return;

	struct list_elem* curr = head;
	struct list_elem* newElem = calloc(1, sizeof(struct list_elem));
	newElem->elem = elem;

	while (curr->next != NULL) {
		curr = curr->next;
	}
	curr->next = newElem;
}

void insert(struct list_elem* head, void* elem, size_t index) {
	if (head == NULL) return;

	struct list_elem* curr = head;
	struct list_elem* newElem = calloc(1, sizeof(struct list_elem));
	newElem->elem = elem;

	size_t i = 0;
	while (curr->next != NULL && i < index) {
		curr = curr->next;
		i++;
	}
	newElem->next = curr->next;
	curr->next = newElem;
}

void* delete(struct list_elem* head, size_t index) {
	if (head == NULL) return NULL;

	struct list_elem* curr = head;
	size_t i = 0;
	while (curr->next != NULL && i < index) {
		curr = curr->next;
		i++;
	}
	if (curr->next == NULL) return NULL;
	void* elem = curr->next->elem;
	struct list_elem* newNext = curr->next->next;
	free(curr->next);
	curr->next = newNext;
	return elem;
}

void* get(struct list_elem* head, size_t index) {
	if (head == NULL) return NULL;

	struct list_elem* curr = head->next;
	size_t i = 0;
	while(curr->next != NULL && i < index) {
		curr = curr->next;
		i++;
	}
	return curr->elem;
}
void set(struct list_elem* head, void* elem, size_t index) {
	if (head == NULL) return;

	struct list_elem* curr = head->next;
	size_t i = 0;
	while(curr->next != NULL && i < index) {
		curr = curr->next;
		i++;
	}
	curr->elem = elem;
}
size_t len(struct list_elem* head) {
	if (head == NULL) return -1;

	struct list_elem* curr = head;
	int i = 0;
	while (curr->next != NULL) {
		curr = curr->next;
		i++;
	}
	return i;
}

void print(struct list_elem* head) {
	if (head == NULL) return;
	for (struct list_elem* curr = head->next; curr != NULL; curr = curr->next) {
		int* value = curr->elem;
		printf("%d ", *value);
	}
	printf("\n");
}
