#ifndef LIST_H
#define LIST_H

struct list_elem {
    void* elem;
    struct list_elem* next;
};

void log_ (char*);
void log_i(int);

void add(struct list_elem*, void*);
void insert(struct list_elem*, void*, size_t);
void* delete(struct list_elem*, size_t);
void* get(struct list_elem*, size_t);
void set(struct list_elem*, void*, size_t);
size_t len(struct list_elem*);
void print(struct list_elem*);

#endif
