#include <stdio.h>
#include <stdlib.h>
#include "list.h"

int main(int argc, char* argv[]) {
	struct list_elem head = {.elem = NULL, .next = NULL};
	int values[10];

	for (int i = 0; i < 10; i++) {
		values[i] = i;
		add(&head, &values[i]);
		//log_i(i);
	}

	log_("initial list");
	print(&head);

	log_("insert 36 at index 5");
	int val1 = 36;
	insert(&head, &val1, 5);
	print(&head);

	log_("insert 10 at index 15");
	int val2 = 10;
	insert(&head, &val2, 15);
	print(&head);

	log_("delete item at index 5");
	delete(&head, 5);
	print(&head);

	log_("delete item at index 13");
	delete(&head, 13);
	print(&head);

	log_("get item at index 8");
	int* item8 = get(&head, 8);
	printf("item: %d\n", *item8);

	log_("set item at index 2 to this value");
	set(&head, item8, 2);
	print(&head);

	log_("the length of the list:");
	log_i(len(&head));
}
