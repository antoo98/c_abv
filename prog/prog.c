#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	if (argc < 2) {
		printf("argument required!\n");
		return EXIT_FAILURE;
	}

	int strlen = 0;
	for (; argv[1][strlen] != '\0'; strlen++);

	char* endptr = &argv[1][strlen];
	//char* endptr = NULL;

	int result = strtoull(argv[1], &endptr, 10);

	if (endptr == &argv[1][strlen]) {
		printf("%d\n", result);
	} else {
		//printf("%d - %d\n", endptr, &argv[1][strlen-1]);
		printf("argument '%s' isn't numeric!\n", argv[1]);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
