#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	for (int c = 0; c < argc; c++) {
		printf("%d: %s\n", c, argv[c]);
	}
	return EXIT_SUCCESS;
}
