#include <stdio.h>
#include <stdlib.h>

#define RED   "\033[31m"
#define GREEN "\033[32m"
#define RESET "\033[0m"

struct point {
	int x;
	int y;
};
struct point* create_p(int, int);
struct point* create_p2(int, int);
void print_p(struct point*);

int main(int argc, char* argv[]) {
	struct point p = {.x = 2, .y = 4};
	struct point q = *create_p(5, 7);
	struct point r = *create_p2(1, 3);

	printf("normal: ");
	print_p(&p);
	printf("malloc: ");
	print_p(&q);
	printf("calloc: ");
	print_p(&r);

	return EXIT_SUCCESS;
}

struct point* create_p(int x, int y) {
	struct point* p = malloc(sizeof(struct point));
	p->x = x;
	p->y = y;
	return p;
}
struct point* create_p2(int x, int y) {
    struct point* p = calloc(1,sizeof(struct point));
    p->x = x;
    p->y = y;
    return p;
}


void print_p(struct point* p) {
	printf("x: "GREEN"%d"RESET", y: "GREEN"%d"RESET"\n", p->x, p->y);
}
