#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	int v = 5;

	printf("v         : %d\n", v);
	printf("size of v : %zu\n", sizeof(v));
	printf("&v        : %p\n", &v);
	printf("size of &v: %zu\n", sizeof(&v));

	return EXIT_SUCCESS;
}
